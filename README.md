# Frog\_eDNA

## eDNA


### Raw reads 

| lib | nb\_reads |
| ----------- | ----------- |
| 1ind15\_S6\_L001\_R1\_001.fastq.gz|135876|
|1ind15\_S6\_L001\_R2\_001.fastq.gz|135876|
|1ind17\_S7\_L001\_R1\_001.fastq.gz|34196|
|1ind17\_S7\_L001\_R2\_001.fastq.gz|34196|
|1ind18\_S8\_L001\_R1\_001.fastq.gz|47445|
|1ind18\_S8\_L001\_R2\_001.fastq.gz|47445|
|1ind27\_S10\_L001\_R1\_001.fastq.gz|174177|
|1ind27\_S10\_L001\_R2\_001.fastq.gz|174177|
|2ind12\_S3\_L001\_R1\_001.fastq.gz|142957|
|2ind12\_S3\_L001\_R2\_001.fastq.gz|142957|
|2ind13\_S4\_L001\_R1\_001.fastq.gz|86961|
|2ind13\_S4\_L001\_R2\_001.fastq.gz|86961|
|2ind14\_S5\_L001\_R1\_001.fastq.gz|24419|
|2ind14\_S5\_L001\_R2\_001.fastq.gz|24419|
|2ind19\_S9\_L001\_R1\_001.fastq.gz|118652|
|2ind19\_S9\_L001\_R2\_001.fastq.gz|118652|
|2ind28\_S11\_L001\_R1\_001.fastq.gz|120256|
|2ind28\_S11\_L001\_R2\_001.fastq.gz|120256|
|3ind10\_S1\_L001\_R1\_001.fastq.gz|101745|
|3ind10\_S1\_L001\_R2\_001.fastq.gz|101745|
|3ind11\_S2\_L001\_R1\_001.fastq.gz|101188|
|3ind11\_S2\_L001\_R2\_001.fastq.gz|101188|

Quality checking with FASTQC
```
for i in `ls *gz` ; do  fastqc "$i" ; done
```

Example of base content R1 file:
![](base\_content\_1ind15.png)


Example of base content R2 file:
![](base\_content\_1ind15\_R2.png)

Example of adapter in R2 file:
![](base\_content\_1ind15\_R2\_adapter.png)


### Demultiplexing

Barcode list:

| sample | barcode | lib |
| ----------- | ----------- | ----------- |
|SPY\_201\_291|CAACCG|3ind10|
|SPY\_201\_292|CAACCG|3ind11|
|SPY\_201\_293|CAACCG|2ind12|
|SPY\_201\_294|CAACCG|2ind13|
|SPY\_201\_295|CAACCG|2ind14|
|SPY\_211\_196|CAACCG|2ind19|
|SPY\_211\_206|CAACCG|1ind27|
|SPY\_211\_211|CAACCG|2ind28|
|SPY\_211\_212|GTATGA|3ind10|
|SPY\_201\_664|GTATGA|3ind11|
|SPY\_211\_201|GTATGA|2ind12|
|SPY\_211\_203|GTATGA|2ind13|
|SPY\_211\_208|GTATGA|2ind14|
|SPY\_211\_195|GTATGA|1ind15|
|SPY\_211\_200|GTATGA|1ind17|
|SPY\_211\_205|GTATGA|1ind18|
|SPY\_211\_210|GTATGA|2ind19|
|SPY\_211\_204|GTATGA|2ind28|
|SPY\_211\_213|TGGATT|3ind10|
|SPY\_211\_214|TGGATT|3ind11|

script\_demux.sh

```
# !/bin/bash

source /local/anaconda3/bin/activate /home/jeremy/local/envcutadapt2/

awk '{print $3}' list\_barcodes | sort | uniq > list\_lib

while read a
	do
	grep "$a" list\_barcodes | awk '{print ">"$1"#^"$2}' | tr "#" "\n" > temp\_barcodes.fasta
	cutadapt -e 0.17 --minimum-length 30 -q 10 --no-indels -g file:temp\_barcodes.fasta -o demux-{name}\_R1\_.fastq.gz -p demux-{name}\_R2\_.fastq.gz "$a"\_*\_R1\_001.fastq.gz "$a"\_*\_R2\_001.fastq.gz 
	done < list\_lib
```

| sample | nb\_reads |
| ----------- | ----------- |
|demux-SPY\_201\_291|21028|
|demux-SPY\_201\_292|44078|
|demux-SPY\_201\_293|85990|
|demux-SPY\_201\_294|91190|
|demux-SPY\_201\_295|16548|
|demux-SPY\_201\_664|97872|
|demux-SPY\_211\_195|263464|
|demux-SPY\_211\_196|109164|
|demux-SPY\_211\_200|67740|
|demux-SPY\_211\_201|189650|
|demux-SPY\_211\_203|80550|
|demux-SPY\_211\_204|20092|
|demux-SPY\_211\_205|92242|
|demux-SPY\_211\_206|342230|
|demux-SPY\_211\_208|23708|
|demux-SPY\_211\_210|114118|
|demux-SPY\_211\_211|217400|
|demux-SPY\_211\_212|105428|
|demux-SPY\_211\_213|75164|
|demux-SPY\_211\_214|58404|

**mean 105803 reads (sd 85684)**

At the end, **97.5%** of the reads have been associated to a sample

### Reads cleaning


```
# !/bin/bash

source /local/anaconda3/bin/activate /home/jeremy/local/envcutadapt2/

for i in `ls demux-*\_R1\_.fastq.gz`
	do
	cutadapt -q 10 -m 30 -a AGATCGGAAGAGC -o clean\_"$i" "$i"
	done


for i in `ls demux-*\_R2\_.fastq.gz`
	do
	cutadapt -u 5 -q 10 -m 30 -a AGATCGGAAGAGC -o clean\_"$i" "$i"
	done

source /local/anaconda3/bin/activate /home/jeremy/local/envfastqpair

for i in `ls clean\_demux-*\_R1\_.fastq.gz`
		do
		name=`echo $i | sed -e 's/\_R1\_.fastq.gz//g'`
		gzip -d "$i"
		gzip -d "$name"\_R2\_.fastq.gz
		fastq\_pair "$name"\_R1\_.fastq "$name"\_R2\_.fastq
		done
```
Process: 

* remove base with quality lower than 10
* remove reads shoter than 30bp
* remove adapter starting by AGATCGGAAGAGC
* synchro R1 R2


### Mapping on genome
Genome from NCBI:
**GCF\_905171775.1\_aRanTem1.1\_genomic.fna**

| sample | nb\_reads\_mapped | % |
| ----------- | ----------- | ----------- |
|SPY\_201\_291\_on\_ref.sam|20534|94.92%|
|SPY\_201\_292\_on\_ref.sam|44286|97.80%|
|SPY\_201\_293\_on\_ref.sam|85751|97.15%|
|SPY\_201\_294\_on\_ref.sam|91111|97.10%|
|SPY\_201\_295\_on\_ref.sam|16205|95.37%|
|SPY\_201\_664\_on\_ref.sam|99032|98.20%|
|SPY\_211\_195\_on\_ref.sam|265664|97.74%|
|SPY\_211\_196\_on\_ref.sam|107946|97.24%|
|SPY\_211\_200\_on\_ref.sam|68882|98.25%|
|SPY\_211\_201\_on\_ref.sam|190024|97.94%|
|SPY\_211\_203\_on\_ref.sam|80559|97.23%|
|SPY\_211\_204\_on\_ref.sam|20068|96.85%|
|SPY\_211\_205\_on\_ref.sam|93927|98.53%|
|SPY\_211\_206\_on\_ref.sam|341571|97.66%|
|SPY\_211\_208\_on\_ref.sam|23988|98.32%|
|SPY\_211\_210\_on\_ref.sam|114803|98.06%|
|SPY\_211\_211\_on\_ref.sam|216696|97.11%|
|SPY\_211\_212\_on\_ref.sam|106207|97.74%|
|SPY\_211\_213\_on\_ref.sam|75819|98.13%|
|SPY\_211\_214\_on\_ref.sam|59123|98.03%|

## SAMPLES
same process

### Demultiplexing

| sample | barcode | lib |
| ----------- | ----------- | ----------- |
|RTE-01|TGGATT|8ind12|
|RTE-02|TGGATT|8ind13|
|RTE-03|TGGATT|5ind14|
|RTE-04|TGGATT|7ind15|
|RTE-05|TGGATT|6ind17|
|RTE-06|TGGATT|7ind18|
|RTE-07|TGGATT|7ind19|
|RTE-08|TGGATT|7ind27|
|RTE-09|TGGATT|7ind28|
|RTE-10|CCAGCT|7ind10|
|RTE-11|CCAGCT|7ind11|
|RTE-12|CCAGCT|8ind12|
|RTE-13|CCAGCT|8ind13|
|RTE-15|CCAGCT|7ind15|
|RTE-16|CCAGCT|6ind17|
|RTE-17|CCAGCT|7ind18|
|RTE-18|CCAGCT|7ind19|
|RTE-19|CCAGCT|7ind27|
|RTE-20|CCAGCT|7ind28|
|RTE-21|AACTCG|7ind10|
|RTE-22|AACTCG|7ind11|
|RTE-23|AACTCG|8ind12|
|RTE-24|AACTCG|8ind13|
|RTE-25|AACTCG|5ind14|
|RTE-26|AACTCG|7ind15|
|RTE-27|AACTCG|6ind17|
|RTE-28|AACTCG|7ind18|
|RTE-29|AACTCG|7ind19|
|RTE-30|AACTCG|7ind27|
|RTE-31|AACTCG|7ind28|
|RTE-32|ACCAGA|7ind10|
|RTE-33|ACCAGA|7ind11|
|RTE-34|ACCAGA|8ind12|
|RTE-35|ACCAGA|8ind13|
|RTE-36|ACCAGA|5ind14|
|RTE-37|ACCAGA|7ind15|
|RTE-38|ACCAGA|6ind17|
|RTE-39|ACCAGA|7ind18|
|RTE-40|ACCAGA|7ind19|
|RTE-41|ACCAGA|7ind27|
|RTE-42|ACCAGA|7ind28|
|RTE-43|CAATTC|7ind10|
|RTE-44|CAATTC|7ind11|
|RTE-45|CAATTC|8ind12|
|RTE-46|CAATTC|8ind13|
|RTE-47|CAATTC|5ind14|
|RTE-48|CAATTC|7ind15|
|RTE-49|CAATTC|6ind17|
|RTE-50|CAATTC|7ind18|
|RTE-51|CAATTC|7ind19|
|RTE-52|CAATTC|7ind27|
|RTE-53|CAATTC|7ind28|
|RTE-54|CTGCTG|7ind10|
|RTE-55|CTGCTG|7ind11|
|RTE-56|CTGCTG|8ind12|
|RTE-57|CTGCTG|8ind13|
|RTE-59|CTGCTG|7ind15|
|RTE-61|CTGCTG|7ind18|
|RTE-62|CTGCTG|7ind19|
|RTE-63|CTGCTG|7ind27|
|RTE-64|CTGCTG|7ind28|
|RTE-65|CAACCG|7ind10|
|RTE-66|CAACCG|7ind11|
|RTE-67|CAACCG|8ind12|
|RTE-68|CAACCG|8ind13|
|RTE-69|CAACCG|5ind14|
|RTE-70|CAACCG|7ind15|
|RTE-71|CAACCG|6ind17|
|RTE-72|CAACCG|7ind18|
|RTE-73|CAACCG|7ind19|
|RTE-74|CAACCG|7ind27|
|RTE-75|CAACCG|7ind28|
|RTE-76|GTATGA|7ind10|
|RTE-77|GTATGA|7ind11|
|RTE-78|GTATGA|8ind12|
|RTE-79|GTATGA|8ind13|


Number of reads

| sample | nb\_reads |
| ----------- | ----------- |
|demux-RTE-01|7838|
|demux-RTE-02|8694|
|demux-RTE-03|8478|
|demux-RTE-04|10848|
|demux-RTE-05|6452|
|demux-RTE-06|9256|
|demux-RTE-07|5740|
|demux-RTE-08|9714|
|demux-RTE-09|8034|
|demux-RTE-10|29032|
|demux-RTE-11|22718|
|demux-RTE-12|25742|
|demux-RTE-13|25352|
|demux-RTE-15|28220|
|demux-RTE-16|20206|
|demux-RTE-17|29054|
|demux-RTE-18|34068|
|demux-RTE-19|30352|
|demux-RTE-20|27306|
|demux-RTE-21|21906|
|demux-RTE-22|16108|
|demux-RTE-23|19658|
|demux-RTE-24|14924|
|demux-RTE-25|25948|
|demux-RTE-26|30960|
|demux-RTE-27|35986|
|demux-RTE-28|28848|
|demux-RTE-29|20900|
|demux-RTE-30|19534|
|demux-RTE-31|15506|
|demux-RTE-32|21218|
|demux-RTE-33|23034|
|demux-RTE-34|27350|
|demux-RTE-35|29360|
|demux-RTE-36|31188|
|demux-RTE-37|26302|
|demux-RTE-38|14932|
|demux-RTE-39|13242|
|demux-RTE-40|27168|
|demux-RTE-41|28462|
|demux-RTE-42|29926|
|demux-RTE-43|36338|
|demux-RTE-44|38644|
|demux-RTE-45|36066|
|demux-RTE-46|34714|
|demux-RTE-47|32604|
|demux-RTE-48|27292|
|demux-RTE-49|28838|
|demux-RTE-50|46182|
|demux-RTE-51|48676|
|demux-RTE-52|43206|
|demux-RTE-53|39060|
|demux-RTE-54|19806|
|demux-RTE-55|22506|
|demux-RTE-56|16350|
|demux-RTE-57|17624|
|demux-RTE-59|23310|
|demux-RTE-61|11928|
|demux-RTE-62|9358|
|demux-RTE-63|8704|
|demux-RTE-64|8176|
|demux-RTE-65|25766|
|demux-RTE-66|21524|
|demux-RTE-67|20018|
|demux-RTE-68|26396|
|demux-RTE-69|23546|
|demux-RTE-70|19530|
|demux-RTE-71|15322|
|demux-RTE-72|23006|
|demux-RTE-73|14054|
|demux-RTE-74|18438|
|demux-RTE-75|20934|
|demux-RTE-76|23788|
|demux-RTE-77|21340|
|demux-RTE-78|16256|
|demux-RTE-79|12854|

**mean 22654 reads (sd 9690)**

At the end, **96.0%** of the reads have been associated to a sample


### Mapping on genome
Genome from NCBI:
**GCF\_905171775.1\_aRanTem1.1\_genomic.fna**


| sample | nb\_reads\_mapped | % |
| ----------- | ----------- | ----------- |
|RTE-01\_on\_ref.sam|7974|98.27%|
|RTE-02\_on\_ref.sam|8912|98.58%|
|RTE-03\_on\_ref.sam|8613|98.25%|
|RTE-04\_on\_ref.sam|11000|98.28%|
|RTE-05\_on\_ref.sam|6570|98.55%|
|RTE-06\_on\_ref.sam|9409|98.67%|
|RTE-07\_on\_ref.sam|5842|98.80%|
|RTE-08\_on\_ref.sam|9832|98.00%|
|RTE-09\_on\_ref.sam|8011|96.62%|
|RTE-10\_on\_ref.sam|29008|96.89%|
|RTE-11\_on\_ref.sam|22669|97.53%|
|RTE-12\_on\_ref.sam|25765|97.54%|
|RTE-13\_on\_ref.sam|25311|96.94%|
|RTE-15\_on\_ref.sam|28177|96.96%|
|RTE-16\_on\_ref.sam|20196|97.32%|
|RTE-17\_on\_ref.sam|29323|98.45%|
|RTE-18\_on\_ref.sam|34171|97.30%|
|RTE-19\_on\_ref.sam|30388|97.21%|
|RTE-20\_on\_ref.sam|27437|97.30%|
|RTE-21\_on\_ref.sam|21971|97.32%|
|RTE-22\_on\_ref.sam|16177|97.45%|
|RTE-23\_on\_ref.sam|19730|97.22%|
|RTE-24\_on\_ref.sam|14855|96.83%|
|RTE-25\_on\_ref.sam|26388|98.24%|
|RTE-26\_on\_ref.sam|31478|97.90%|
|RTE-27\_on\_ref.sam|36767|98.32%|
|RTE-28\_on\_ref.sam|29324|98.40%|
|RTE-29\_on\_ref.sam|21154|97.62%|
|RTE-30\_on\_ref.sam|19721|97.81%|
|RTE-31\_on\_ref.sam|15637|97.77%|
|RTE-32\_on\_ref.sam|21488|98.34%|
|RTE-33\_on\_ref.sam|23217|97.33%|
|RTE-34\_on\_ref.sam|27540|97.76%|
|RTE-35\_on\_ref.sam|29395|96.58%|
|RTE-36\_on\_ref.sam|31351|97.24%|
|RTE-37\_on\_ref.sam|26602|97.61%|
|RTE-38\_on\_ref.sam|15041|97.35%|
|RTE-39\_on\_ref.sam|13391|98.01%|
|RTE-40\_on\_ref.sam|27288|97.45%|
|RTE-41\_on\_ref.sam|28632|97.87%|
|RTE-42\_on\_ref.sam|30024|97.38%|
|RTE-43\_on\_ref.sam|36766|98.15%|
|RTE-44\_on\_ref.sam|39073|97.76%|
|RTE-45\_on\_ref.sam|36616|98.12%|
|RTE-46\_on\_ref.sam|35058|97.86%|
|RTE-47\_on\_ref.sam|32957|97.98%|
|RTE-48\_on\_ref.sam|27781|98.24%|
|RTE-49\_on\_ref.sam|29397|98.89%|
|RTE-50\_on\_ref.sam|46740|99.08%|
|RTE-51\_on\_ref.sam|49486|98.10%|
|RTE-52\_on\_ref.sam|43915|98.55%|
|RTE-53\_on\_ref.sam|39517|98.30%|
|RTE-54\_on\_ref.sam|20192|98.81%|
|RTE-55\_on\_ref.sam|22902|98.43%|
|RTE-56\_on\_ref.sam|16508|98.97%|
|RTE-57\_on\_ref.sam|17879|98.53%|
|RTE-59\_on\_ref.sam|23875|98.97%|
|RTE-61\_on\_ref.sam|12150|98.67%|
|RTE-62\_on\_ref.sam|9525|98.18%|
|RTE-63\_on\_ref.sam|8824|98.18%|
|RTE-64\_on\_ref.sam|8252|97.99%|
|RTE-65\_on\_ref.sam|26110|98.31%|
|RTE-66\_on\_ref.sam|21797|98.14%|
|RTE-67\_on\_ref.sam|20326|98.56%|
|RTE-68\_on\_ref.sam|26494|97.29%|
|RTE-69\_on\_ref.sam|23827|98.12%|
|RTE-70\_on\_ref.sam|19747|98.15%|
|RTE-71\_on\_ref.sam|15616|98.51%|
|RTE-72\_on\_ref.sam|23239|98.52%|
|RTE-73\_on\_ref.sam|14078|97.20%|
|RTE-74\_on\_ref.sam|18539|97.56%|
|RTE-75\_on\_ref.sam|20761|95.99%|
|RTE-76\_on\_ref.sam|24011|97.55%|
|RTE-77\_on\_ref.sam|21292|96.61%|
|RTE-78\_on\_ref.sam|16281|97.16%|
|RTE-79\_on\_ref.sam|12857|96.65%|
